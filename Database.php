<?php 

class Database{
 
function __construct() {
//echo "<p>". __CLASS__ ." Class construct </p>";
}

function __destruct() {
//echo "<p>".__CLASS__ . " Class destruct </p>";
}
    
public function hello(){
echo " ".__CLASS__ ." Class ";
}
    
private $host = "localhost";
private $user = "";
private $pass = "";
private $database ="";

private $link;
protected $result;

public function mysqli_db_connect(){
$this->link = mysqli_connect($this->host, $this->user, $this->pass, $this->database);
if (mysqli_connect_errno($this->link)) {
echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
return $mysqli=$this->link;
}

public function mysqli_db_query($sql,$link){
$this->result = mysqli_query($link, $sql);
if(!$this->result) echo "Query failed! ";

return $this->result;

if (mysqli_insert_id($link)){
return mysqli_insert_id($link);
}

}

public function count_results($result){
return mysqli_num_rows($result);
}

public function array_result($data){
return mysqli_fetch_array($data);
}

public function object_result($value){
return mysqli_fetch_object($value);
}

public function mysqli_db_close(){
mysqli_close($this->link);
mysqli_free_result($this->result);
}

}
?>